package agents;

import OSPABA.*;
import continualAssistants.BusIn;
import continualAssistants.BusOut;
import continualAssistants.MoveBus;
import entities.Minibus;
import java.util.ArrayList;
import java.util.List;
import simulation.*;
import managers.*;

//meta! id="14"
public class AgentBus extends Agent {
    private ManagerBus managerBus;
    private BusIn busin;
    private BusOut busout;
    private MoveBus moveb;
    private List<Minibus> buses;

    public AgentBus(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication        
        Minibus bus;
        buses.clear();
        for (int i=0;i<Globals.BUS_NUMBER;i++){
            bus = new Minibus(Globals.BUS_CAPACITY[Globals.BUS_TYPE],i);
            buses.add(bus);
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        managerBus=new ManagerBus(Id.managerBus, mySim(), this);
        busin=new BusIn(Id.busIn, mySim(), this);
        busout=new BusOut(Id.busOut, mySim(), this);
        moveb=new MoveBus(Id.moveBus, mySim(), this);
        addOwnMessage(Mc.init);
        addOwnMessage(Mc.custToBus);
        addOwnMessage(Mc.moveBus);
        addOwnMessage(Mc.busInFinish);
        addOwnMessage(Mc.busOutFinish);
        addOwnMessage(Mc.serviceIsWorking);
        buses=new ArrayList<>();        
    }
    //meta! tag="end"

    public List<Minibus> getBuses() {
        return buses;
    }
}