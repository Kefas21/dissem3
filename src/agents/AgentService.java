package agents;

import OSPABA.*;
import OSPDataStruct.SimQueue;
import OSPStat.Stat;
import continualAssistants.ServeCust;
import entities.Employee;
import entities.Group;
import java.util.ArrayList;
import java.util.List;
import simulation.*;
import managers.*;

//meta! id="13"
public class AgentService extends Agent {
    private ManagerService managerServ;
    private ServeCust serveCust;
    private List<Employee> service;
    private SimQueue<Group> queue;
    private SimQueue<Group> outQueue;
    private Stat rental;
    private Stat term3;
    private Stat rentalTime;
    private Stat termTime3;

    public AgentService(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        queue.clear();
        outQueue.clear();
        Employee e;
        service.clear();
        for (int i=0;i<Globals.EMPLOYEE_NUMBER;i++){
            e=new Employee(i);
            service.add(e);
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        managerServ=new ManagerService(Id.managerService, mySim(), this);
        serveCust=new ServeCust(Id.serveCust,mySim(), this);
        addOwnMessage(Mc.arrivalCustRent);
        addOwnMessage(Mc.getCust);
        addOwnMessage(Mc.busOutFinish);
        addOwnMessage(Mc.serveCustFinish);
        addOwnMessage(Mc.serviceIsWorking);
        addOwnMessage(Mc.busesStopped);
        service= new ArrayList<>();
        queue = new SimQueue<>();
        outQueue = new SimQueue<>();
        rental = new Stat();
        term3 = new Stat();
        rentalTime = new Stat();
        termTime3 = new Stat();
    }
    //meta! tag="end"

    public SimQueue<Group> getQueue() {
        return queue;
    }

    public SimQueue<Group> getOutQueue() {
        return outQueue;
    }
    
    public void setFreeService(){
        for (Employee e:service){
            e.setFree(true);
        }
    }
    
    public int getSizeT3(){
        int size=0;
        for (Group g:outQueue){
            size+=g.getQueue().size();
        }
        return size;
    }
    
    /**
     *
     * @return
     */
    public boolean isEmployeeAvailable() {
        for (Employee e : service) {
            if (e.isFree()) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    public Employee employeeAvailable() {
        for (Employee e : service) {
            if (e.isFree()) {
                return e;
            }
        }
        return null;
    }
    
    public boolean isWorking(){
        for (Employee e : service) {
            if (!e.isFree()) {
                return true;
            }
        }
        return false;
    }

    public List<Employee> getService() {
        return service;
    }

    public void setService(List<Employee> service) {
        this.service = service;
    }

    public Stat getRental() {
        return rental;
    }

    public Stat getTerm3() {
        return term3;
    }

    public Stat getRentalTime() {
        return rentalTime;
    }

    public Stat getTermTime3() {
        return termTime3;
    }
}