package agents;

import OSPABA.*;
import simulation.*;
import managers.*;

//meta! id="1"
public class AgentBoss extends Agent {

    private ManagerBoss managerBoss;

    public AgentBoss(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        managerBoss = new ManagerBoss(Id.managerBoss, mySim(), this);
        addOwnMessage(Mc.init);
    }
    //meta! tag="end"

    public void startSim() {
        MyMessage msg = new MyMessage(mySim());
        msg.setCode(Mc.init);
        msg.setAddressee(Id.agentBoss);
        manager().notice(msg);
    }
}
