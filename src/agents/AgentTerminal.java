package agents;

import OSPABA.*;
import OSPDataStruct.SimQueue;
import OSPStat.Stat;
import entities.Group;
import simulation.*;
import managers.*;

//meta! id="20"
public class AgentTerminal extends Agent {

    private ManagerTerminal managerTerm;
    private SimQueue<Group> terminal1;
    private SimQueue<Group> terminal2;
    private Stat term1;
    private Stat term2;
    private Stat termTime1;
    private Stat termTime2;

    public AgentTerminal(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        terminal1.clear();
        terminal2.clear();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        managerTerm = new ManagerTerminal(Id.managerTerminal, mySim(), this);
        addOwnMessage(Mc.arrivalCustT1);
        addOwnMessage(Mc.arrivalCustT2);
        addOwnMessage(Mc.getCust);
        addOwnMessage(Mc.busOutFinish);
        terminal1 = new SimQueue<>();
        terminal2 = new SimQueue<>();
        term1 = new Stat();
        term2 = new Stat();
        termTime1 = new Stat();
        termTime2 = new Stat();
    }
    //meta! tag="end"

    public int getSizeT1() {
        int size = 0;
        for (Group g : terminal1) {
            size += g.getQueue().size();
        }
        return size;
    }

    public int getSizeT2() {
        int size = 0;
        for (Group g : terminal2) {
            size += g.getQueue().size();
        }
        return size;
    }

    public SimQueue<Group> getTerminal1() {
        return terminal1;
    }

    public SimQueue<Group> getTerminal2() {
        return terminal2;
    }

    public Stat getTerm1() {
        return term1;
    }

    public Stat getTerm2() {
        return term2;
    }

    public Stat getTermTime1() {
        return termTime1;
    }

    public Stat getTermTime2() {
        return termTime2;
    }

}
