package agents;

import OSPABA.*;
import OSPStat.Stat;
import simulation.*;
import managers.*;
import continualAssistants.*;

//meta! id="2"
public class AgentSur extends Agent {
    
    private ManagerSur managerSur;
    private ArrivalT1 arrivT1;
    private ArrivalT2 arrivT2;
    private ArrivalRent arrivRent;    
    private Stat timeInSystemIn; 
    private Stat timeInSystemOut; 


    public AgentSur(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        managerSur=new ManagerSur(Id.managerSur, mySim(), this);
        arrivT1=new ArrivalT1(Id.arrivalT1, mySim(), this);
        arrivRent=new ArrivalRent(Id.arrivalRent, mySim(), this);
        arrivT2=new ArrivalT2(Id.arrivalT2, mySim(), this);
        timeInSystemIn=new Stat();
        timeInSystemOut=new Stat();
        addOwnMessage(Mc.init);
        addOwnMessage(Mc.newCust);
        addOwnMessage(Mc.arrivalCust);
        addOwnMessage(Mc.exitCust);
    }
    //meta! tag="end"

    public Stat getTimeInSystemIn() {
        return timeInSystemIn;
    }

    public Stat getTimeInSystemOut() {
        return timeInSystemOut;
    }
}