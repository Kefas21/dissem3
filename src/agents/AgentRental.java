package agents;

import OSPABA.*;
import simulation.*;
import managers.*;

//meta! id="3"
public class AgentRental extends Agent {

    private ManagerRental managerRent;

    public AgentRental(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        managerRent = new ManagerRental(Id.managerRental, mySim(), this);
    }
    //meta! tag="end"
}
