/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package continualAssistants;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Scheduler;
import OSPABA.Simulation;
import OSPRNG.UniformContinuousRNG;
import entities.Customer;
import simulation.Globals;
import simulation.Mc;
import simulation.MyMessage;

/**
 *
 * @author peter
 */
public class BusIn extends Scheduler {
    private UniformContinuousRNG uniGen = new UniformContinuousRNG(Globals.CUSTOMER_INPUT_MIN, Globals.CUSTOMER_INPUT_MAX);

    public BusIn(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message.createCopy();
        switch (message.code()) {
            case Mc.start:
                msg.getBus().getQueue().add(msg.getGroup());
                msg.getBus().setState("Customer getting in");
                double holdTime=0;
                for(Customer c:msg.getGroup().getQueue()){
                    holdTime+=uniGen.sample();
                    msg.getBus().incCustCounter();
                }
                msg.setCode(Mc.busInFinish);
                hold(holdTime,msg);
                break;
                
            case Mc.busInFinish:
                assistantFinished(message);
                break;
        }
    }
    
}
