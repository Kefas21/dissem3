/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package continualAssistants;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Scheduler;
import OSPABA.Simulation;
import simulation.Globals;
import simulation.Mc;
import simulation.MyMessage;

/**
 *
 * @author peter
 */
public class MoveBus extends Scheduler {

    public MoveBus(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message.createCopy();
        switch (message.code()) {
            case Mc.start:
                String state = "";
                switch (msg.getBusStopId()) {
                    case 0:
                    case 2:
                        state = "On way to T1";
                        break;
                    case 1:
                        state = "On way to T3";
                        break;
                    case 3:
                        state = "On way to T2";
                        //ak pride bus plny z T1 na T2, pripocitaj mu plnu obsadenost pre statistiky
                        if (msg.getBus().isFull())
                            msg.getBus().addCustToCounter(msg.getBus().getSize());
                        break;
                    case 4:
                        state = "On way to Rental";
                        break;
                }
                msg.getBus().setState(state);
                msg.getBus().incDistance(Globals.BUS_STOP_DISTANCE[msg.getBusStopId()]);
                //ak bus ide na T1, musi byt prazdny, tuto jazdu nepocitam do statistik ohladom vytazenia
                if (msg.getBusStopId()!=2 || msg.getBusStopId()!=0)
                msg.getBus().incRouteCounter();                
                msg.setCode(Mc.moveBus);
                hold(Globals.BUS_STOP_TIME[msg.getBusStopId()], msg);
                break;
            case Mc.moveBus:
                //System.out.println("Bus prisiel do destinacie "+msg.getBusStopId()+" v case "+mySim().currentTime());
                assistantFinished(message);
                break;
        }
    }
}
