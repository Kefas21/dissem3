package continualAssistants;

import OSPABA.*;
import OSPRNG.ExponentialRNG;
import OSPRNG.UniformDiscreteRNG;
import agents.AgentSur;
import entities.Group;
import simulation.*;

//meta! id="35"
public class ArrivalRent extends Scheduler {

    private ExponentialRNG[] expGen;
    private UniformDiscreteRNG uniGenRent = new UniformDiscreteRNG(0, 100);
    int counter;

    public ArrivalRent(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
        expGen = new ExponentialRNG[Globals.RENTAL_ARRIVAL.length];
        for (int i = 0; i < expGen.length; i++) {
            expGen[i] = new ExponentialRNG(Globals.RENTAL_ARRIVAL[i]);
        }
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        counter=0;
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message.createCopy();
        switch (message.code()) {
            case Mc.start:
                msg.setCode(Mc.newCust);
                hold(expGen[0].sample(), msg);
                break;

            case Mc.newCust:
                double time;
                int gSize = getGroupSize();
                msg.setCode(Mc.arrivalCust);
                msg.setGroup(new Group(gSize, mySim().currentTime(), true));
                msg.setAddressee(myAgent());
                notice(msg);

                if (Globals.SIM_WARMANDOPEN_TIME > mySim().currentTime()) {
                    time = getArrivalTime();
                    if (time > Globals.SIM_WARMANDOPEN_TIME) {
                        time = Globals.SIM_WARMANDOPEN_TIME;
                    }
                    MyMessage cp = (MyMessage) message.createCopy();
                    cp.setCode(Mc.newCust);
                    hold(time, cp);
                    break;
                } else {
                    assistantFinished(message);
                    break;
                }
        }
    }
    //meta! tag="end"
    
    public double getArrivalTime() {
        double generated;
        int interval;
        if (mySim().currentTime() <= Globals.SIM_WARMING_TIME) {
            interval = 0;
            generated = expGen[interval].sample();
        } else {
            double time = mySim().currentTime() - Globals.SIM_WARMING_TIME;
            interval = (int) (time / Globals.SIM_INTERVAL_GAP);
            generated = expGen[interval].sample();
        }
        double genTime = generated + mySim().currentTime();
        double indexBound = (Globals.SIM_INTERVAL_GAP * interval) + Globals.SIM_INTERVAL_GAP + Globals.SIM_WARMING_TIME;
        double overhang;
        double scale;
        while (genTime > indexBound) {
            indexBound += Globals.SIM_INTERVAL_GAP;
            interval++;
            if (interval >= Globals.RENTAL_ARRIVAL.length) {
                return Globals.SIM_WARMANDOPEN_TIME- mySim().currentTime();
            }
            overhang = genTime - (indexBound - Globals.SIM_INTERVAL_GAP);
            scale = overhang * (Globals.RENTAL_ARRIVAL[interval - 1] / Globals.RENTAL_ARRIVAL[interval]);
            genTime = scale + indexBound - Globals.SIM_INTERVAL_GAP;
        }
            return genTime - mySim().currentTime();
    }

    public int getGroupSize() {
        int gSize = 0,
                number = uniGenRent.sample();
        if (number < 60) {
            gSize = 1;
        } else if (60 <= number && number < 80) {
            gSize = 2;
        } else if (80 <= number && number < 95) {
            gSize = 3;
        } else if (95 <= number) {
            gSize = 4;
        }
        return gSize;
    }

    @Override
    public AgentSur myAgent() {
        return (AgentSur) super.myAgent();
    }
}
