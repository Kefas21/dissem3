/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package continualAssistants;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Scheduler;
import OSPABA.Simulation;
import OSPRNG.UniformContinuousRNG;
import entities.Customer;
import simulation.Globals;
import simulation.Mc;
import simulation.MyMessage;

/**
 *
 * @author peter
 */
public class BusOut extends Scheduler {
    private UniformContinuousRNG uniGen = new UniformContinuousRNG(Globals.CUSTOMER_OUTPUT_MIN, Globals.CUSTOMER_OUTPUT_MAX);

    public BusOut(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message.createCopy();
        switch (message.code()) {
            case Mc.start:
                msg.setGroup(msg.getBus().getQueue().poll());
                msg.getBus().setState("Customer getting out");
                double holdTime=0;
                for(Customer c:msg.getGroup().getQueue()){
                    holdTime+=uniGen.sample();
                }                
                msg.setCode(Mc.busOutFinish);
                hold(holdTime,msg);
                break;
                
            case Mc.busOutFinish:
                assistantFinished(msg);
                break;
        }
    }
    
}
