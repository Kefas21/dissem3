/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package continualAssistants;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Scheduler;
import OSPABA.Simulation;
import OSPRNG.TriangularRNG;
import OSPRNG.UniformContinuousRNG;
import agents.AgentService;
import simulation.Mc;
import simulation.MyMessage;

/**
 *
 * @author peter
 */
public class ServeCust extends Scheduler {
    private UniformContinuousRNG uniGenIn;
    private TriangularRNG trnGenInFirst;
    private TriangularRNG trnGenInSecond;
    
    private UniformContinuousRNG uniGenOut;
    private TriangularRNG trnGenOutFirst;
    private TriangularRNG trnGenOutSecond;
    
    public ServeCust(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
        uniGenIn=new UniformContinuousRNG(0.0,100.0);
        uniGenOut=new UniformContinuousRNG(0.0,100.0);
        trnGenInFirst = new TriangularRNG(1.45*60, 1.99*60, 3.25*60);
        trnGenInSecond = new TriangularRNG(3.01*60, 4.68*60, 5.29*60);
        trnGenOutFirst = new TriangularRNG(0.999*60, 1.31*60, 2.21*60);
        trnGenOutSecond = new TriangularRNG(2.71*60, 4.3*60, 4.99*60);
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }
    
    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message;
        switch (message.code()) {
            case Mc.start:
                msg.getEmpl().setFree(false);
                msg.getEmpl().setState("Serving");
                double holdTime=0;
                if (msg.getGroup().getQueue().peek().isReturnCar())
                    holdTime=randomOut();
                else holdTime=randomIn();
                msg.getEmpl().setLoadStartTime(holdTime);
                msg.setCode(Mc.serveCustFinish);
                hold(holdTime, msg);
                break;
                
            case Mc.serveCustFinish:
                msg.getEmpl().incLoadTime(msg.getEmpl().getLoadStartTime());
                assistantFinished(msg);
                break;
        }
    }
    
    public double randomIn(){
        if (uniGenIn.sample()<(63900.0/832.0)){
            return trnGenInFirst.sample();
        } else return trnGenInSecond.sample();
    }
    
    public double randomOut(){
        if (uniGenOut.sample()<(84700.0/978.0)){
            return trnGenOutFirst.sample();
        } else return trnGenOutSecond.sample();
    }
    
    @Override
    public AgentService myAgent() {
        return (AgentService) super.myAgent();
    }
}