/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dissem3;

import entities.Employee;
import entities.Minibus;
import java.util.List;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import simulation.BusMessage;
import simulation.EmplMessage;
import simulation.Globals;
import simulation.MySimulation;

/**
 *
 * @author peter
 */
public class DISsem3 extends Application {

    private MySimulation rc = new MySimulation();
    private LineChart<Number, Number> busLinechart;
    private LineChart<Number, Number> serviceLinechart;
    private XYChart.Series<Number, Number> busSeriesIn;
    private XYChart.Series<Number, Number> serviceSeriesIn;
    private XYChart.Series<Number, Number> busSeriesOut;
    private XYChart.Series<Number, Number> serviceSeriesOut;
    private ObservableList<EmplMessage> emplData;
    private ObservableList<BusMessage> busData;
    private int reps = 1000;
    private int sims = 0;
    private int obsBus;
    private int obsEmp;
    private int obsBusType;
    private Label simCount;
    private Label repCount;
    private Button startBtn;
    private Button pauseBtn;
    private Button stopBtn;
    private Label simStateLabel;
    private Label timeLabel;
    private Label q1Label;
    private Label q1TimeLabel;
    private Label q2TimeLabel;
    private Label q3TimeLabel;
    private Label q4TimeLabel;
    private Label q2Label;
    private Label q3Label;
    private Label q4Label;
    private Label BEcounter;
    private Label avgTimeInLabel;
    private Label avgTimeOutLabel;
    private Label rightInIS;
    private Label leftInIS;
    private Label rightOutIS;
    private Label leftOutIS;
    private Label busCosts;
    private Label driverCosts;
    private Label emplCosts;
    private TableView emplTable;
    private TableView busTable;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("AirCarRental");
        primaryStage.setWidth(1800);
        primaryStage.setHeight(850);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(50);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        //Sim options
        Label optsLabel = new Label("Simulation options");
        grid.add(optsLabel, 0, 0);

        Label minBusNumberLabel = new Label("Min number of buses:");
        grid.add(minBusNumberLabel, 0, 1);
        TextField minBusNumberField = new TextField();
        minBusNumberField.setText("1");
        minBusNumberField.setMaxWidth(30);
        grid.add(minBusNumberField, 1, 1);

        Label maxBusNumberLabel = new Label("Max number of buses:");
        grid.add(maxBusNumberLabel, 0, 2);
        TextField maxBusNumberField = new TextField();
        maxBusNumberField.setText("10");
        maxBusNumberField.setMaxWidth(30);
        grid.add(maxBusNumberField, 1, 2);

        Label minSerNumberLabel = new Label("Min number of employees:");
        grid.add(minSerNumberLabel, 0, 3);
        TextField minSerNumberField = new TextField();
        minSerNumberField.setText("1");
        minSerNumberField.setMaxWidth(30);
        grid.add(minSerNumberField, 1, 3);

        Label maxSerNumberLabel = new Label("Min number of employees:");
        grid.add(maxSerNumberLabel, 0, 4);
        TextField maxSerNumberField = new TextField();
        maxSerNumberField.setText("10");
        maxSerNumberField.setMaxWidth(30);
        grid.add(maxSerNumberField, 1, 4);

        //Observation options
        Label obsLabel = new Label("Observe options");
        grid.add(obsLabel, 2, 0);

        Label obsEmpNumberLabel = new Label("Employee number to observe:");
        grid.add(obsEmpNumberLabel, 2, 1);
        TextField obsEmpNumberField = new TextField();
        obsEmpNumberField.setText("3");
        obsEmpNumberField.setMaxWidth(30);
        grid.add(obsEmpNumberField, 3, 1);

        Label obsBusNumberLabel = new Label("Bus number to observe:");
        grid.add(obsBusNumberLabel, 2, 2);
        TextField obsBusNumberField = new TextField();
        obsBusNumberField.setText("3");
        obsBusNumberField.setMaxWidth(30);
        grid.add(obsBusNumberField, 3, 2);

        Label obsBusTypeLabel = new Label("Bus type to observe:");
        grid.add(obsBusTypeLabel, 2, 3);
        ChoiceBox obsBusTypeCB = new ChoiceBox(FXCollections.observableArrayList(
                "A", "B", "C"));
        obsBusTypeCB.setValue(obsBusTypeCB.getItems().get(0));
        obsBusTypeCB.setMaxWidth(50);
        grid.add(obsBusTypeCB, 3, 3);

        //Sim speed options
        Label speedLabel = new Label("Simulation speed");
        grid.add(speedLabel, 4, 0);
        ToggleGroup speedGroup = new ToggleGroup();
        RadioButton normalSpeedRadio = new RadioButton("Fast");
        normalSpeedRadio.setToggleGroup(speedGroup);
        grid.add(normalSpeedRadio, 4, 1);
        RadioButton slowSpeedRadio = new RadioButton("Slow");
        slowSpeedRadio.setSelected(true);
        slowSpeedRadio.setToggleGroup(speedGroup);
        grid.add(slowSpeedRadio, 4, 2);
        RadioButton superslowSpeedRadio = new RadioButton("Super Slow");
        superslowSpeedRadio.setToggleGroup(speedGroup);
        grid.add(superslowSpeedRadio, 4, 3);

        //Sim startAsync/pause/stop options
        Label actionLabel = new Label("Actions");
        grid.add(actionLabel, 5, 0);
        startBtn = new Button("Start");
        pauseBtn = new Button("Pause");
        stopBtn = new Button("Stop");
        startBtn.setMinWidth(100);
        pauseBtn.setMinWidth(100);
        stopBtn.setMinWidth(100);
        pauseBtn.setDisable(true);
        stopBtn.setDisable(true);
        grid.add(startBtn, 5, 1);
        grid.add(pauseBtn, 5, 2);
        grid.add(stopBtn, 5, 3);

        speedGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
            if (speedGroup.getSelectedToggle() == slowSpeedRadio) {
                rc.setSimSpeed(100, 0.5);
            }
            if (speedGroup.getSelectedToggle() == normalSpeedRadio) {
                rc.setMaxSimSpeed();
            }
            if (speedGroup.getSelectedToggle() == superslowSpeedRadio) {
                rc.setSimSpeed(1, 1);
            }
        });

        //Sim time info
        Label simInfoLabel = new Label("Simulation Info");
        grid.add(simInfoLabel, 6, 0);
        Label simLabel = new Label("Simulation:");
        simCount = new Label();
        grid.add(simLabel, 6, 1);
        grid.add(simCount, 7, 1);
        Label repLabel = new Label("Replication:");
        repCount = new Label();
        grid.add(repLabel, 6, 2);
        grid.add(repCount, 7, 2);
        Label simState = new Label("Simulation state:");
        simStateLabel = new Label();
        grid.add(simState, 6, 3);
        grid.add(simStateLabel, 7, 3);
        Label tLabel = new Label("Replication time:");
        timeLabel = new Label();
        grid.add(tLabel, 6, 4);
        grid.add(timeLabel, 7, 4);

        //Queue stats
        Label avgInfoLabel = new Label("Average Queue Statistics");
        grid.add(avgInfoLabel, 0, 6);
        Label q1InfoLabel = new Label("Average queue T1:");
        grid.add(q1InfoLabel, 0, 7);
        q1Label = new Label();
        grid.add(q1Label, 1, 7);
        Label q2InfoLabel = new Label("Average queue T2:");
        grid.add(q2InfoLabel, 0, 8);
        q2Label = new Label();
        grid.add(q2Label, 1, 8);
        Label q3InfoLabel = new Label("Average queue T3:");
        grid.add(q3InfoLabel, 0, 9);
        q3Label = new Label();
        grid.add(q3Label, 1, 9);
        Label q4InfoLabel = new Label("Average queue Rental:");
        grid.add(q4InfoLabel, 0, 10);
        q4Label = new Label();
        grid.add(q4Label, 1, 10);

        //queue time stats
        Label timeInfoLabel = new Label("Average Queue Time Statistics");
        grid.add(timeInfoLabel, 2, 6);
        Label q1tInfoLabel = new Label("Average time in queue T1:");
        grid.add(q1tInfoLabel, 2, 7);
        q1TimeLabel = new Label();
        grid.add(q1TimeLabel, 3, 7);
        Label q2tInfoLabel = new Label("Average time in queue T2:");
        grid.add(q2tInfoLabel, 2, 8);
        q2TimeLabel = new Label();
        grid.add(q2TimeLabel, 3, 8);
        Label q3tInfoLabel = new Label("Average time in queue T3:");
        grid.add(q3tInfoLabel, 2, 9);
        q3TimeLabel = new Label();
        grid.add(q3TimeLabel, 3, 9);
        Label q4tInfoLabel = new Label("Average time in queue Rental:");
        grid.add(q4tInfoLabel, 2, 10);
        q4TimeLabel = new Label();
        grid.add(q4TimeLabel, 3, 10);

        //system time stats
        Label systemInfoLabel = new Label("Average System Time Statistics");
        grid.add(systemInfoLabel, 4, 6);
        Label counterLabel = new Label("Employees/Buses/Bus Type:");
        grid.add(counterLabel, 4, 7);
        BEcounter = new Label();
        grid.add(BEcounter, 5, 7);
        Label avgLabel = new Label("Average time for incoming customers:");
        grid.add(avgLabel, 4, 8);
        avgTimeInLabel = new Label();
        grid.add(avgTimeInLabel, 5, 8);
        Label leftISLabel = new Label("Confidence interval left:");
        grid.add(leftISLabel, 4, 9);
        leftInIS = new Label();
        grid.add(leftInIS, 5, 9);
        Label rightISLabel = new Label("Confidence interval right:");
        grid.add(rightISLabel, 4, 10);
        rightInIS = new Label();
        grid.add(rightInIS, 5, 10);
        Label avgOutLabel = new Label("Average time for outcoming customers:");
        grid.add(avgOutLabel, 4, 11);
        avgTimeOutLabel = new Label();
        grid.add(avgTimeOutLabel, 5, 11);
        Label leftOutISLabel = new Label("Confidence interval left:");
        grid.add(leftOutISLabel, 4, 12);
        leftOutIS = new Label();
        grid.add(leftOutIS, 5, 12);
        Label rightOutISLabel = new Label("Confidence interval right:");
        grid.add(rightOutISLabel, 4, 13);
        rightOutIS = new Label();
        grid.add(rightOutIS, 5, 13);

        //finance statistics
        Label finLabel = new Label("Finance Statistics (scaled to 30 days) (€)");
        grid.add(finLabel, 6, 6);
        Label busLabel = new Label("Bus costs:");
        grid.add(busLabel, 6, 7);
        busCosts = new Label();
        grid.add(busCosts, 7, 7);
        Label driverLabel = new Label("Driver costs:");
        grid.add(driverLabel, 6, 8);
        driverCosts = new Label();
        grid.add(driverCosts, 7, 8);
        Label emplLabel = new Label("Employee costs:");
        grid.add(emplLabel, 6, 9);
        emplCosts = new Label();
        grid.add(emplCosts, 7, 9);

        //charts
        setUpLineChart();
        grid.add(busLinechart, 0, 5, 2, 1);
        grid.add(serviceLinechart, 2, 5, 2, 1);

        setUpEmplTable();
        setUpBusTable();
        grid.add(emplTable, 4, 5, 2, 1);
        grid.add(busTable, 6, 5, 2, 1);

        startBtn.setOnAction((ActionEvent event) -> {
            if (rc.isPaused()) {
                rc.resumeSimulation();
                startBtn.setDisable(true);
                pauseBtn.setDisable(false);
                stopBtn.setDisable(false);
                simStateLabel.setText("Running");
            } else {
                int minB = Integer.parseInt(minBusNumberField.getText());
                int maxB = Integer.parseInt(maxBusNumberField.getText());
                int minE = Integer.parseInt(minSerNumberField.getText());
                int maxE = Integer.parseInt(maxSerNumberField.getText());
                obsBus = Integer.parseInt(obsBusNumberField.getText());
                obsEmp = Integer.parseInt(obsEmpNumberField.getText());
                if (obsBusTypeCB.getValue().equals("A")) {
                    obsBusType = 0;
                } else if (obsBusTypeCB.getValue().equals("B")) {
                    obsBusType = 1;
                } else if (obsBusTypeCB.getValue().equals("C")) {
                    obsBusType = 2;
                }

                if (minB <= maxB && minE <= maxE) {
                    startBtn.setDisable(true);
                    pauseBtn.setDisable(false);
                    stopBtn.setDisable(false);
                    rc = new MySimulation(minE, maxE, minB, maxB, reps);
                    sims = (((maxE - minE) + 1) * ((maxB - minB) + 1)) * 3;

                    rc.onRefreshUI((t) -> {
                        refreshBus(((MySimulation) t).agentBus().getBuses());
                        refreshEmpl(((MySimulation) t).agentService().getService());
                        refreshTime(rc.currentTime());
                        refreshReps(rc.currentReplication());
                        refreshSims();
                        refreshQueueStat();
                        refreshQueueTimeStat();
                        refreshSimStat();
                    });

                    rc.onReplicationDidFinish((t) -> {
                        refreshTime(0);
                        refreshReps(rc.currentReplication());
                        refreshSims();
                        refreshQueueStat();
                        refreshQueueTimeStat();
                        refreshSimStat();
                        refreshFinStat();
                    });

                    rc.onSimulationDidFinish((t) -> {
                        refreshCharts();
                    });

                    rc.setSimSpeed(100.0, 0.5);
                    slowSpeedRadio.setSelected(true);
                    normalSpeedRadio.setSelected(false);
                    superslowSpeedRadio.setSelected(false);
                    rc.startAsync();
                    //rc.setSimSpeed(1000.0, 0.5);
                    simStateLabel.setText("Running");
                }
            }
        });

        pauseBtn.setOnAction((ActionEvent event) -> {
            rc.pauseSimulation();
            startBtn.setDisable(false);
            pauseBtn.setDisable(true);
            simStateLabel.setText("Paused");
        });

        stopBtn.setOnAction((ActionEvent event) -> {
            rc.stopSimulation();
            rc.setStopped(true);
            slowSpeedRadio.setSelected(true);
            normalSpeedRadio.setSelected(false);
            superslowSpeedRadio.setSelected(false);
            startBtn.setDisable(false);
            stopBtn.setDisable(true);
            pauseBtn.setDisable(true);
            simStateLabel.setText("Stopped");
        });

        Scene scene = new Scene(grid);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private void setUpLineChart() {
        //set up chart
        NumberAxis xbAxis = new NumberAxis();
        xbAxis.setLabel("Time (min)");
        xbAxis.setAutoRanging(true);
        NumberAxis ybAxis = new NumberAxis();
        ybAxis.setLabel("Employees");
        ybAxis.setAutoRanging(true);
        ybAxis.forceZeroInRangeProperty().set(false);

        NumberAxis xsAxis = new NumberAxis();
        xsAxis.setLabel("Time (min)");
        xsAxis.setAutoRanging(true);
        NumberAxis ysAxis = new NumberAxis();
        ysAxis.setLabel("Buses");
        ysAxis.setAutoRanging(true);
        ysAxis.forceZeroInRangeProperty().set(false);

        busLinechart = new LineChart<>(xbAxis, ybAxis);
        serviceLinechart = new LineChart<>(xsAxis, ysAxis);
        busLinechart.setCreateSymbols(true);
        serviceLinechart.setCreateSymbols(true);
        this.busSeriesIn = new XYChart.Series<>();
        this.serviceSeriesIn = new XYChart.Series<>();
        busSeriesIn.setName("Average Time Input");
        serviceSeriesIn.setName("Average Time Input");
        this.busSeriesOut = new XYChart.Series<>();
        this.serviceSeriesOut = new XYChart.Series<>();
        busSeriesOut.setName("Average Time Output");
        serviceSeriesOut.setName("Average Time Output");

        busLinechart.getData().add(busSeriesIn);
        serviceLinechart.getData().add(serviceSeriesIn);
        busLinechart.getData().add(busSeriesOut);
        serviceLinechart.getData().add(serviceSeriesOut);
    }

    /**
     * Clear all chart data
     */
    public void clearCharts() {
        busSeriesIn.getData().clear();
        serviceSeriesIn.getData().clear();
        busSeriesOut.getData().clear();
        serviceSeriesOut.getData().clear();
    }

    public void setUpEmplTable() {
        //employee table
        emplTable = new TableView();
        emplTable.setMaxHeight(300);
        TableColumn idCol = new TableColumn("Employee ID");
        TableColumn stateCol = new TableColumn("State");
        TableColumn loadCol = new TableColumn("Load (%)");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        stateCol.setCellValueFactory(new PropertyValueFactory<>("state"));
        loadCol.setCellValueFactory(new PropertyValueFactory<>("load"));
        idCol.setMinWidth(10);
        stateCol.setMinWidth(150);
        loadCol.setMinWidth(70);
        emplData = FXCollections.observableArrayList();
        emplTable.setItems(emplData);
        emplTable.getColumns().addAll(idCol, stateCol, loadCol);
    }

    public void setUpBusTable() {
        //bus table
        busTable = new TableView();
        busTable.setMaxHeight(300);
        TableColumn idCol = new TableColumn("Bus ID");
        TableColumn stateCol = new TableColumn("State");
        TableColumn loadCol = new TableColumn("Load (%)");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        stateCol.setCellValueFactory(new PropertyValueFactory<>("state"));
        loadCol.setCellValueFactory(new PropertyValueFactory<>("load"));
        idCol.setMinWidth(10);
        stateCol.setMinWidth(150);
        loadCol.setMinWidth(70);
        busData = FXCollections.observableArrayList();
        busTable.setItems(busData);
        busTable.getColumns().addAll(idCol, stateCol, loadCol);
    }

    public void refreshEmpl(List<Employee> l) {
        Platform.runLater(() -> {
            emplData.clear();
            for (Employee e : l) {
                double load = (e.getLoadTime() / rc.currentTime()) * 100;
                EmplMessage m = new EmplMessage(e.getId(), e.getState(), load);
                emplData.add(m);
            }
        });
    }

    public void refreshBus(List<Minibus> l) {
        Platform.runLater(() -> {
            busData.clear();
            for (Minibus e : l) {
                double load = ((double) e.getCustCounter() / (double) (e.getRouteCounter() * e.getSize())) * 100;
                BusMessage m = new BusMessage(e.getId(), e.getState(), load);
                busData.add(m);
            }
        });
    }

    public void refreshTime(double ct) {
        Platform.runLater(() -> {
            timeLabel.setText(TimeConverter.converter(ct + (3600 * 12)));
        });
    }

    public void refreshReps(int cr) {
        Platform.runLater(() -> {
            repCount.setText(cr + 1 + "/" + reps);
        });
    }

    public void refreshSims() {
        Platform.runLater(() -> {
            simCount.setText(rc.getSims() + "/" + sims);
            if (rc.getSims() == sims) {
                simStateLabel.setText("Done");
            }
        });
    }

    public void refreshQueueStat() {
        Platform.runLater(() -> {
            q1Label.setText(String.valueOf(rc.agentTerminal().getTerm1().mean()));
            q2Label.setText(String.valueOf(rc.agentTerminal().getTerm2().mean()));
            q3Label.setText(String.valueOf(rc.agentService().getTerm3().mean()));
            q4Label.setText(String.valueOf(rc.agentService().getRental().mean()));
        });
    }

    public void refreshQueueTimeStat() {
        Platform.runLater(() -> {
            q1TimeLabel.setText(TimeConverter.onlyTimeConverter(rc.agentTerminal().getTermTime1().mean()));
            q2TimeLabel.setText(TimeConverter.onlyTimeConverter(rc.agentTerminal().getTermTime2().mean()));
            q3TimeLabel.setText(TimeConverter.onlyTimeConverter(rc.agentService().getTermTime3().mean()));
            q4TimeLabel.setText(TimeConverter.onlyTimeConverter(rc.agentService().getRentalTime().mean()));
        });
    }

    public void refreshSimStat() {
        Platform.runLater(() -> {
            String busType = "";
            switch (Globals.BUS_TYPE) {
                case 0:
                    busType = "A";
                    break;
                case 1:
                    busType = "B";
                    break;
                case 2:
                    busType = "C";
                    break;
            }
            BEcounter.setText(Globals.EMPLOYEE_NUMBER + "/" + Globals.BUS_NUMBER + "/" + busType);
            avgTimeInLabel.setText(TimeConverter.onlyTimeConverter(rc.agentSur().getTimeInSystemIn().mean()));
            if (rc.agentSur().getTimeInSystemIn().sampleSize() > 2) {
                leftInIS.setText(TimeConverter.onlyTimeConverter(rc.agentSur().getTimeInSystemIn().confidenceInterval_90()[0]));
                rightInIS.setText(TimeConverter.onlyTimeConverter(rc.agentSur().getTimeInSystemIn().confidenceInterval_90()[1]));
            }
            avgTimeOutLabel.setText(TimeConverter.onlyTimeConverter(rc.agentSur().getTimeInSystemOut().mean()));
            if (rc.agentSur().getTimeInSystemOut().sampleSize() > 2) {
                leftOutIS.setText(TimeConverter.onlyTimeConverter(rc.agentSur().getTimeInSystemOut().confidenceInterval_90()[0]));
                rightOutIS.setText(TimeConverter.onlyTimeConverter(rc.agentSur().getTimeInSystemOut().confidenceInterval_90()[1]));
            }
        });
    }

    public void refreshFinStat() {
        Platform.runLater(() -> {
            busCosts.setText(String.valueOf(rc.getBusCost().mean()));
            driverCosts.setText(String.valueOf(rc.getDriverCost().mean()));
            emplCosts.setText(String.valueOf(rc.getEmplCost().mean()));
        });
    }

    public void refreshCharts() {
        if (Globals.BUS_NUMBER == obsBus && Globals.BUS_TYPE == obsBusType) {
            double timeIn = rc.agentSur().getTimeInSystemIn().mean();
            double timeOut = rc.agentSur().getTimeInSystemOut().mean();
            Platform.runLater(() -> {
                busSeriesIn.getData().add(new XYChart.Data<>(timeIn / 60.0, Globals.EMPLOYEE_NUMBER));
                busSeriesOut.getData().add(new XYChart.Data<>(timeOut / 60.0, Globals.EMPLOYEE_NUMBER));
            });
        }
        if (Globals.EMPLOYEE_NUMBER == obsEmp && Globals.BUS_TYPE == obsBusType) {
            double timeIn = rc.agentSur().getTimeInSystemIn().mean();
            double timeOut = rc.agentSur().getTimeInSystemOut().mean();
            Platform.runLater(() -> {
                serviceSeriesIn.getData().add(new XYChart.Data<>(timeIn / 60.0, Globals.BUS_NUMBER));
                serviceSeriesOut.getData().add(new XYChart.Data<>(timeOut / 60.0, Globals.BUS_NUMBER));
            });
        }
    }
}
