/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author peter
 */
public class Employee {
    private boolean free;
    private int id;
    private String state;
    private double loadTime;
    private double loadStartTime;
    private double workingTime;

    /**
     *
     */
    public Employee() {
        this.free=true;
        this.state="Free";
        this.loadStartTime=0;
        this.loadTime=0;
        this.workingTime=0;
    }
    
    public Employee(int id){
        this.free=true;
        this.id=id;
        this.state="Free";
        this.loadStartTime=0;
        this.loadTime=0;
        this.workingTime=0;
    }
    
    public void incLoadTime(double t){
        loadTime+=t;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     */
    public boolean isFree() {
        return free;
    }

    /**
     *
     * @param free
     */
    public void setFree(boolean free) {
        this.free = free;
    }   

    public double getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(double loadTime) {
        this.loadTime = loadTime;
    }

    public double getLoadStartTime() {
        return loadStartTime;
    }

    public void setLoadStartTime(double loadStartTime) {
        this.loadStartTime = loadStartTime;
    }

    public double getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(double workingTime) {
        this.workingTime = workingTime;
    }
    
}
