/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author peter
 */
public class Group {

    private Queue<Customer> queue;
    private double queueStartTime;

    public Group(int size, double time, boolean retCar) {
        queue = new LinkedList<>();
        Customer c = new Customer(time, retCar);
        for (int i = 0; i < size; i++) {
            queue.add(c);
        }
    }

    public Queue<Customer> getQueue() {
        return queue;
    }

    /**
     *
     * @return
     */
    public double getQueueStartTime() {
        return queueStartTime;
    }

    /**
     *
     * @param queueStartTime
     */
    public void setQueueStartTime(double queueStartTime) {
        this.queueStartTime = queueStartTime;
    }
}
