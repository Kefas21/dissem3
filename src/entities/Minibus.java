/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author peter
 */
public class Minibus {
    private Queue<Group> queue;
    private int size;
    private int id;
    private String state;
    private int routeCounter;
    private int custCounter;
    private double distance;
    private double driveTime;

    /**
     *
     * @param size
     */
    public Minibus(int size, int id) {
        this.queue=new LinkedList<>();
        this.size=size;
        this.id=id;
        this.state="";
        this.custCounter=0;
        this.routeCounter=0;
        this.distance=0;
        this.driveTime=0;
    }
    
    public Minibus(int size) {
        this.queue=new LinkedList<>();
        this.size=size;
        this.state="";
        this.custCounter=0;
        this.routeCounter=0;
        this.distance=0;
        this.driveTime=0;
    }
    
    public int getQueueSize(){
        int size=0;
        for (Group g:queue){
            size+=g.getQueue().size();
        }
        return size;
    }
    
    public void incRouteCounter(){
        routeCounter++;
    }
    
    public void incCustCounter(){
        custCounter++;
    }
    
    public void addCustToCounter(int cust){
        custCounter+=cust;
    }

    /**
     *
     * @return
     */
    public Queue<Group> getQueue() {
        return queue;
    }

    /**
     *
     * @param queue
     */
    public void setQueue(Queue<Group> queue) {
        this.queue = queue;
    } 

    /**
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }
    
    /**
     *
     * @return
     */
    public int getFreeSize(){
        return size-getQueueSize();
    }
    
    /**
     *
     * @return
     */
    public boolean isFull(){
        return getQueueSize()==size;
    }
    
    /**
     *
     * @return
     */
    public boolean isEmpty(){
        return queue.isEmpty();
    }
    
    public void incDistance(double d){
        distance+=d;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getRouteCounter() {
        return routeCounter;
    }

    public void setRouteCounter(int routeCounter) {
        this.routeCounter = routeCounter;
    }

    public int getCustCounter() {
        return custCounter;
    }

    public void setCustCounter(int custCounter) {
        this.custCounter = custCounter;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDriveTime() {
        return driveTime;
    }

    public void setDriveTime(double driveTime) {
        this.driveTime = driveTime;
    }
}