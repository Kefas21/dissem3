/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author peter
 */
public class Customer {

    private double incomeTime;
    private boolean returnCar;

    /**
     *
     * @param time
     */
    public Customer(double time) {
        this.incomeTime = time;
        this.returnCar = false;
    }

    public Customer(double time, boolean returnCar) {
        this.incomeTime = time;
        this.returnCar = returnCar;
    }

    /**
     *
     * @return
     */
    public double getIncomeTime() {
        return incomeTime;
    }

    /**
     *
     * @param incomeTime
     */
    public void setIncomeTime(double incomeTime) {
        this.incomeTime = incomeTime;
    }

    public boolean isReturnCar() {
        return returnCar;
    }

    public void setReturnCar(boolean returnCar) {
        this.returnCar = returnCar;
    }
}
