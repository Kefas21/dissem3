package simulation;

import OSPABA.*;

public class Id extends IdList
{
	//meta! userInfo="Generated code: do not modify", tag="begin"
	public static final int agentBoss = 1;
	public static final int agentSur = 2;
	public static final int agentRental = 3;
	public static final int agentTerminal = 9;
	public static final int agentService = 6;
	public static final int agentBus = 7;
	public static final int managerBoss = 101;
	public static final int managerSur = 102;
	public static final int managerRental = 103;
	public static final int managerTerminal = 109;
	public static final int managerService = 106;
	public static final int managerBus = 107;
	public static final int arrivalT2 = 1002;
	public static final int arrivalRent = 1003;
	public static final int arrivalT1 = 1001;
        public static final int rentalOpen = 1004;
        public static final int serveCust = 1005;
        public static final int busIn = 1006;
        public static final int busOut = 1007;
        public static final int moveBus = 1008;
	//meta! tag="end"
}