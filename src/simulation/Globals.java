/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulation;

/**
 *
 * @author peter
 */
public class Globals {

    /**
     *
     */
    public static final double[] T1_ARRIVAL = new double[]{
        3600 / 4,
        3600 / 8,
        3600 / 12,
        3600 / 15,
        3600 / 18,
        3600 / 14,
        3600 / 13,
        3600 / 10,
        3600 / 4,
        3600 / 6,
        3600 / 10,
        3600 / 14,
        3600 / 16,
        3600 / 15,
        3600 / 7,
        3600 / 3,
        3600 / 4,
        3600 / 2};

    /**
     *
     */
    public static final double[] T2_ARRIVAL = new double[]{
        3600 / 3,
        3600 / 6,
        3600 / 9,
        3600 / 15,
        3600 / 17,
        3600 / 19,
        3600 / 14,
        3600 / 6,
        3600 / 3,
        3600 / 4,
        3600 / 21,
        3600 / 14,
        3600 / 19,
        3600 / 12,
        3600 / 5,
        3600 / 2,
        3600 / 3,
        3600 / 3};

    /**
     *
     */
    public static final double[] RENTAL_ARRIVAL = new double[]{
        3600 / 12,
        3600 / 9,
        3600 / 18,
        3600 / 28,
        3600 / 23,
        3600 / 21,
        3600 / 16,
        3600 / 11,
        3600 / 17,
        3600 / 22,
        3600 / 36,
        3600 / 24,
        3600 / 32,
        3600 / 16,
        3600 / 13,
        3600 / 13,
        3600 / 5,
        3600 / 4};

    /**
     *
     */
    public static final double CUSTOMER_INPUT_MIN = 10;

    /**
     *
     */
    public static final double CUSTOMER_INPUT_MAX = 14;

    /**
     *
     */
    public static final double CUSTOMER_OUTPUT_MIN = 2;

    /**
     *
     */
    public static final double CUSTOMER_OUTPUT_MAX = 10;

    public static final double DRIVER_SALARY = 12.5;

    public static final double SERVICE_SALARY = 11.5;

    /**
     *
     */
    public static final int[] BUS_CAPACITY = {12, 18, 30};
    public static final double[] BUS_PRICE = {0.28, 0.43, 0.54};

    /**
     *
     */
    public static final double AVERAGE_BUS_SPEED = 35 / 3.6;

    /**
     *
     */
    public static final double[] BUS_STOP_TIME = {
        2500 / AVERAGE_BUS_SPEED,
        2900 / AVERAGE_BUS_SPEED,
        900 / AVERAGE_BUS_SPEED,
        500 / AVERAGE_BUS_SPEED,
        3400 / AVERAGE_BUS_SPEED
    };
    
    public static final double[] BUS_STOP_DISTANCE = {
        2.500,
        2.900,
        0.900,
        0.500,
        3.400
    };
    
    /**
     *
     */
    public static final double STUDENT_ALFA_90 = 1.645;

    /**
     *
     */
    public static final double SIM_START_TIME = 0;
    public static final double SIM_WARMING_TIME = 3600 * 4;
    public static final double RENTAL_OPEN_TIME = 4.5 * 3600;
    public static final double SIM_WARMANDOPEN_TIME = SIM_WARMING_TIME + RENTAL_OPEN_TIME;
    public static final double SIM_INTERVAL_GAP = 900;
    public static int BUS_NUMBER;
    public static int EMPLOYEE_NUMBER;
    public static int BUS_TYPE;
}
