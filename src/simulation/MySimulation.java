package simulation;

import OSPABA.*;
import OSPStat.Stat;
import agents.*;
import entities.Employee;
import entities.Minibus;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MySimulation extends Simulation {

    private int minBusNumber;
    private int minEmployeeNumber;
    private int maxBusNumber;
    private int maxEmployeeNumber;
    private int reps;
    private boolean stopped;
    private int sims;
    private Stat busCost;
    private Stat driverCost;
    private Stat emplCost;
    private Map excel;

    public MySimulation(int minE, int maxE, int minB, int maxB, int reps) {
        minEmployeeNumber = minE;
        maxEmployeeNumber = maxE;
        minBusNumber = minB;
        maxBusNumber = maxB;
        this.reps = reps;
        sims = 0;
        stopped = false;
        busCost = new Stat();
        driverCost = new Stat();
        emplCost = new Stat();
        this.excel = new TreeMap<String, Object[]>();
        init();
    }

    public MySimulation() {
        init();
    }

    @Override
    public void prepareSimulation() {
        super.prepareSimulation();
        // Create global statistcis
        _agentTerminal.getTerm1().clear();
        _agentTerminal.getTerm2().clear();
        _agentTerminal.getTermTime1().clear();
        _agentTerminal.getTermTime2().clear();
        _agentService.getTerm3().clear();
        _agentService.getRental().clear();
        _agentService.getTermTime3().clear();
        _agentService.getRentalTime().clear();
        _agentSur.getTimeInSystemIn().clear();
        _agentSur.getTimeInSystemOut().clear();
        busCost.clear();
        driverCost.clear();
        emplCost.clear();

    }

    @Override
    public void prepareReplication() {
        //System.out.println(" --- New Replication nr. "+currentReplication()+" --- ");
        super.prepareReplication();
        // Reset entities, queues, local statistics, etc...
        _agentBoss.startSim();
    }

    @Override
    public void replicationFinished() {
        // Collect local statistics into global, update UI, etc...
        super.replicationFinished();

        double driverPrice = 0;
        double busPrice = 0;
        for (Minibus b : _agentBus.getBuses()) {
            driverPrice += b.getDriveTime();
            busPrice += b.getDistance();
        }

        double emplPrice = 0;
        for (Employee e : _agentService.getService()) {
            emplPrice += e.getWorkingTime();
        }

        driverCost.addSample(driverPrice * Globals.DRIVER_SALARY * 30);
        busCost.addSample(busPrice * Globals.BUS_PRICE[Globals.BUS_TYPE] * 30);
        emplCost.addSample(emplPrice * Globals.SERVICE_SALARY * 30);
    }

    @Override
    public void simulationFinished() {
        // Dysplay simulation results
        super.simulationFinished();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        setAgentBoss(new AgentBoss(Id.agentBoss, this, null));
        setAgentSur(new AgentSur(Id.agentSur, this, agentBoss()));
        setAgentRental(new AgentRental(Id.agentRental, this, agentBoss()));
        setAgentTerminal(new AgentTerminal(Id.agentTerminal, this, agentRental()));
        setAgentService(new AgentService(Id.agentService, this, agentRental()));
        setAgentBus(new AgentBus(Id.agentBus, this, agentRental()));
    }

    public void startAsync() {
        Thread thread = new Thread(() -> {
            for (int i = minEmployeeNumber; i <= maxEmployeeNumber; i++) { //employees
                if (stopped) {
                    break;
                }
                for (int j = minBusNumber; j <= maxBusNumber; j++) { //buses
                    if (stopped) {
                        break;
                    }
                    for (int k = 0; k < 3; k++) { //bus types
                        if (stopped) {
                            break;
                        }
                        Globals.BUS_TYPE = k;
                        Globals.BUS_NUMBER = j;
                        Globals.EMPLOYEE_NUMBER = i;
                        simulate(reps);
                        sims++;
                    }
                }
            }
        });
        thread.setName("SimThread");
        thread.setDaemon(true);
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
    }

    public void startSyncExcel() {
        int hlp = 0;
        for (int i = minEmployeeNumber; i <= maxEmployeeNumber; i++) { //employees
            for (int j = minBusNumber; j <= maxBusNumber; j++) { //buses
                for (int k = 0; k < 3; k++) { //bus types
                    Globals.BUS_TYPE = k;
                    Globals.BUS_NUMBER = j;
                    Globals.EMPLOYEE_NUMBER = i;
                    System.out.println("Busy: " + j + " Typ: " + k + " Obsluha: " + i);
                    simulate(reps);
                    sims++;
                    System.out.println(" --- Simulation nr. " + sims + " --- ");
                    Object[] obj = new Object[]{Globals.EMPLOYEE_NUMBER, Globals.BUS_NUMBER, Globals.BUS_TYPE, 
                        String.valueOf(_agentSur.getTimeInSystemIn().mean()/60.0), String.valueOf(_agentSur.getTimeInSystemIn().confidenceInterval_90()[0]/60.0), String.valueOf(_agentSur.getTimeInSystemIn().confidenceInterval_90()[1]/60.0),
                        String.valueOf(_agentSur.getTimeInSystemOut().mean()/60.0), String.valueOf(_agentSur.getTimeInSystemOut().confidenceInterval_90()[0]/60.0), String.valueOf(_agentSur.getTimeInSystemOut().confidenceInterval_90()[1]/60.0),
                         String.valueOf(busCost.mean()), String.valueOf(driverCost.mean()), String.valueOf(emplCost.mean())
                    };
                    excel.put(String.valueOf(hlp), obj);
                    hlp++;
                }
            }
        }
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");

        //Iterate over data and write to sheet
        Set<String> keyset = excel.keySet();

        int rownum = 0;
        for (String key : keyset) {
            //create a row of excelsheet
            Row row = sheet.createRow(rownum++);

            //get object array of prerticuler key
            Object[] objArr = (Object[]) excel.get(key);

            int cellnum = 0;

            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }
        try {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("results.xlsx"));
            workbook.write(out);
            out.close();
            System.out.println(".xlsx written successfully on disk.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void finishReplication() {
        finishReplication();
    }

    private AgentBoss _agentBoss;

    public AgentBoss agentBoss() {
        return _agentBoss;
    }

    public void setAgentBoss(AgentBoss agentBoss) {
        _agentBoss = agentBoss;
    }

    private AgentSur _agentSur;

    public AgentSur agentSur() {
        return _agentSur;
    }

    public void setAgentSur(AgentSur agentSur) {
        _agentSur = agentSur;
    }

    private AgentRental _agentRental;

    public AgentRental agentRental() {
        return _agentRental;
    }

    public void setAgentRental(AgentRental agentRental) {
        _agentRental = agentRental;
    }

    private AgentTerminal _agentTerminal;

    public AgentTerminal agentTerminal() {
        return _agentTerminal;
    }

    public void setAgentTerminal(AgentTerminal agentTerminal) {
        _agentTerminal = agentTerminal;
    }

    private AgentService _agentService;

    public AgentService agentService() {
        return _agentService;
    }

    public void setAgentService(AgentService agentService) {
        _agentService = agentService;
    }

    private AgentBus _agentBus;

    public AgentBus agentBus() {
        return _agentBus;
    }

    public void setAgentBus(AgentBus agentBus) {
        _agentBus = agentBus;
    }
    //meta! tag="end"

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public int getSims() {
        return sims;
    }

    public Stat getBusCost() {
        return busCost;
    }

    public Stat getDriverCost() {
        return driverCost;
    }

    public Stat getEmplCost() {
        return emplCost;
    }
}
