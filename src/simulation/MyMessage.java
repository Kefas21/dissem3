package simulation;

import OSPABA.*;
import entities.Employee;
import entities.Group;
import entities.Minibus;

public class MyMessage extends MessageForm {

    private Group group;
    private Employee empl;
    private Minibus bus;
    private int busStopId;
    private boolean serviceWorking;

    public MyMessage(Simulation sim) {
        super(sim);
        group = null;
        empl = null;
        bus = null;
        busStopId = -1;
        serviceWorking = false;
    }

    public MyMessage(MyMessage original) {
        super(original);
        // copy() is called in superclass
        group = original.getGroup();
        empl = original.getEmpl();
        bus = original.getBus();
        busStopId = original.getBusStopId();
        serviceWorking = original.isServiceWorking();
    }

    @Override
    public MessageForm createCopy() {
        return new MyMessage(this);
    }

    @Override
    protected void copy(MessageForm message) {
        super.copy(message);
        MyMessage original = (MyMessage) message;
        // Copy attributes
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Employee getEmpl() {
        return empl;
    }

    public void setEmpl(Employee empl) {
        this.empl = empl;
    }

    public Minibus getBus() {
        return bus;
    }

    public void setBus(Minibus bus) {
        this.bus = bus;
    }

    public int getBusStopId() {
        return busStopId;
    }

    public void setBusStopId(int busStopId) {
        this.busStopId = busStopId;
    }

    public boolean isServiceWorking() {
        return serviceWorking;
    }

    public void setServiceWorking(boolean serviceWorking) {
        this.serviceWorking = serviceWorking;
    }
}