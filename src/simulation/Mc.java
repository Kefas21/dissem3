package simulation;

import OSPABA.*;

public class Mc extends IdList {
    //meta! userInfo="Generated code: do not modify", tag="begin"

    public static final int init = 1001;
    public static final int exitCust = 1004;
    public static final int arrivalCust = 1006;
    public static final int arrivalCustT1 = 1008;
    public static final int arrivalCustT2 = 1009;
    public static final int arrivalCustRent = 1002;
    public static final int newCust = 1011;
    public static final int serveInCustomer = 1012;
    public static final int serveOutCustomer = 1013;
    public static final int moveBus = 1014;
    public static final int getCust = 1015;
    public static final int custToBus = 1016;
    public static final int busInFinish = 1017;
    public static final int busOutFinish = 1018;
    public static final int serveCustFinish = 1019;
    public static final int serviceIsWorking = 1020;
    public static final int busesStopped = 1021;
    //meta! tag="end"
}