package managers;

import OSPABA.*;
import simulation.*;
import agents.*;

//meta! id="3"
public class ManagerRental extends Manager {

    public ManagerRental(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentBoss", id="27", type="Notice"
    public void processInit(MessageForm message) {
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        MessageForm cp = message.createCopy();
        switch (message.code()) {
            case Mc.arrivalCustRent:
                cp.setAddressee(Id.agentService);
                notice(cp);
                break;

            case Mc.arrivalCustT1:
            case Mc.arrivalCustT2:
                cp.setAddressee(Id.agentTerminal);
                notice(cp);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentRental myAgent() {
        return (AgentRental) super.myAgent();
    }

}
