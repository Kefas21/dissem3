package managers;

import OSPABA.*;
import OSPDataStruct.SimQueue;
import agents.*;
import entities.Employee;
import entities.Group;
import entities.Minibus;
import simulation.Globals;
import simulation.Id;
import simulation.Mc;
import simulation.MyMessage;

//meta! id="13"
public class ManagerService extends Manager {

    public ManagerService(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message;
        switch (message.code()) {
            case Mc.arrivalCustRent:
                if (myAgent().isEmployeeAvailable()) {
                    msg.setEmpl(myAgent().employeeAvailable());
                    msg.setAddressee(Id.serveCust);
                    startContinualAssistant(msg);
                } else {
                    if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                        myAgent().getRental().addSample(myAgent().getQueue().size());
                    }
                    myAgent().getQueue().add(msg.getGroup());
                    msg.getGroup().setQueueStartTime(mySim().currentTime());
                    if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                        myAgent().getRental().addSample(myAgent().getQueue().size());
                    }
                }
                break;
            case Mc.getCust:
                Group g = null;
                g = findProperGroup(msg.getBus(), myAgent().getOutQueue());
                if (g!=null && g.getQueue().peek().getIncomeTime()>Globals.SIM_WARMING_TIME)
                    myAgent().getTermTime3().addSample(mySim().currentTime()-g.getQueueStartTime());
                msg.setGroup(g);
                msg.setCode(Mc.custToBus);
                response(msg);
                break;
            case Mc.busOutFinish:
                if (myAgent().isEmployeeAvailable()) {
                    msg.setEmpl(myAgent().employeeAvailable());
                    msg.setAddressee(Id.serveCust);
                    startContinualAssistant(msg);
                } else {
                    if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                        myAgent().getRental().addSample(myAgent().getQueue().size());
                    }
                    myAgent().getQueue().add(msg.getGroup());
                    msg.getGroup().setQueueStartTime(mySim().currentTime());
                    if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                        myAgent().getRental().addSample(myAgent().getQueue().size());
                    }
                }
                break;
            case Mc.serviceIsWorking:
                msg.setServiceWorking(myAgent().isWorking());
                response(msg);
                break;
                
                case Mc.busesStopped:
                for (Employee e:myAgent().getService()){
                    e.setState("Closed");
                    e.setWorkingTime((mySim().currentTime()-Globals.SIM_WARMING_TIME)/3600);
                }
                break;

            case Mc.finish:
                switch (message.sender().id()) {
                    case Id.serveCust:
                        msg.getEmpl().setFree(true);
                        msg.getEmpl().setState("Free");
                        if (msg.getGroup().getQueue().peek().isReturnCar()) {
                            if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                                myAgent().getTerm3().addSample(myAgent().getSizeT3());
                            }
                            myAgent().getOutQueue().add(msg.getGroup());
                            msg.getGroup().setQueueStartTime(mySim().currentTime());
                            if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                                myAgent().getTerm3().addSample(myAgent().getSizeT3());
                            }
                        } else {
                            msg.setCode(Mc.exitCust);
                            msg.setAddressee(Id.agentSur);
                            notice(msg);
                        }
                        if (!myAgent().getQueue().isEmpty()) {
                            msg = (MyMessage) msg.createCopy();
                            msg.setGroup(myAgent().getQueue().poll());
                            if (msg.getGroup().getQueue().peek().getIncomeTime()>Globals.SIM_WARMING_TIME)
                                myAgent().getRentalTime().addSample(mySim().currentTime()-msg.getGroup().getQueueStartTime());
                            msg.setAddressee(Id.serveCust);
                            startContinualAssistant(msg);
                        }
                }
        }
    }
    //meta! tag="end"

    public Group findProperGroup(Minibus bus, SimQueue q) {
        Group g = null;
        if (q.size() > 0) {
            for (int i = 0; i < q.size(); i++) {
                g = (Group) q.get(i);
                if (g.getQueue().size() <= bus.getFreeSize()) {
                    q.remove(g);
                    return g;
                }
            }
        } else {
            return null;
        }
        return g;
    }

    @Override
    public AgentService myAgent() {
        return (AgentService) super.myAgent();
    }
}
