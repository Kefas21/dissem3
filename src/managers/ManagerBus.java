package managers;

import OSPABA.*;
import agents.*;
import entities.Minibus;
import simulation.Globals;
import simulation.Id;
import simulation.Mc;
import simulation.MyMessage;

//meta! id="14"
public class ManagerBus extends Manager {

    public ManagerBus(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        MyMessage cp = (MyMessage) message.createCopy();
        switch (message.code()) {
            case Mc.init:
                for (Minibus b : myAgent().getBuses()) {
                    cp = (MyMessage) message.createCopy();
                    cp.setBus(b);
                    cp.setBusStopId(0);
                    cp.setAddressee(Id.moveBus);
                    startContinualAssistant(cp);
                }
                break;

            case Mc.custToBus:
                //zaciatok nastupu
                if (cp.getGroup() != null) {
                    cp.setAddressee(Id.busIn);
                    startContinualAssistant(cp);
                } else {
                    //move bus
                    moveBusToNextStop(cp);
                }
                break;

            case Mc.serviceIsWorking:
                if (cp.isServiceWorking()) {
                    cp.setBusStopId(0);
                    cp.setServiceWorking(false);
                    cp.setAddressee(Id.moveBus);
                    startContinualAssistant(cp);
                } else {
                    cp.getBus().setState("Bus parked");
                    cp.getBus().setDriveTime((mySim().currentTime()-Globals.SIM_WARMING_TIME)/3600);
                    boolean parked=true;
                    for (Minibus m:myAgent().getBuses()){
                        if (!m.getState().equals("Bus parked"))
                            parked=false;
                    }
                    if (parked) {
                        cp=(MyMessage) cp.createCopy();
                        cp.setAddressee(Id.agentService);
                        cp.setCode(Mc.busesStopped);
                        notice(cp);
                    }
                }
                break;

            case Mc.finish:
                switch (message.sender().id()) {
                    case Id.moveBus:
                        //prichod busu na terminal, zaciatok nastupu/vystupu
                        switch (cp.getBusStopId()) {
                            case 0://terminal 1
                            case 2://terminal 1
                            case 3://terminal 2
                                getCustomerFromTerminal(cp);
                                break;
                            case 1:
                                //bus na terminale 3, vykladka ludi
                                if (!cp.getBus().isEmpty()) {
                                    cp.setAddressee(Id.busOut);
                                    startContinualAssistant(cp);
                                }
                                break;
                            case 4:
                                //bus na pozicovni, vykladka a pripadna nakladka
                                if (!cp.getBus().isEmpty()) {
                                    cp.setAddressee(Id.busOut);
                                    startContinualAssistant(cp);
                                } else //chod nalozit ludi
                                {
                                    getCustomerFromRental(cp);
                                }
                                break;
                        }
                        break;
                    case Id.busIn:
                        //koniec nastupu, bud zacat dalsi nastup alebo odchod autobusu
                        if (cp.getBus().getFreeSize() > 0) {
                            if (cp.getBusStopId() == 4) {
                                getCustomerFromRental(cp);
                            } else {
                                getCustomerFromTerminal(cp);
                            }
                        } else {
                            moveBusToNextStop(cp);
                        }
                        break;
                    case Id.busOut:
                        //koniec vystupu, bud zacat dalsi vystup alebo odchod autobusu
                        if (cp.getBusStopId() == 4) {
                            cp.setAddressee(Id.agentService);
                        } else if (cp.getBusStopId() == 1) {
                            cp.setAddressee(Id.agentTerminal);
                        }
                        cp.setCode(Mc.busOutFinish);
                        notice(cp);

                        cp = (MyMessage) cp.createCopy();
                        if (!cp.getBus().isEmpty()) {
                            cp.setAddressee(Id.busOut);
                            startContinualAssistant(cp);
                        } else if (cp.getBusStopId() == 4) //chod nalozit ludi
                        {
                            getCustomerFromRental(cp);
                        } else {
                            moveBusToNextStop(cp);
                        }
                        break;
                }
                break;
        }
    }
    //meta! tag="end"

    public void getCustomerFromTerminal(MyMessage cp) {
        cp.setAddressee(Id.agentTerminal);
        cp.setCode(Mc.getCust);
        request(cp);
    }

    public void getCustomerFromRental(MyMessage cp) {
        cp.setAddressee(Id.agentService);
        cp.setCode(Mc.getCust);
        request(cp);
    }

    public void moveBusToNextStop(MyMessage cp) {
        switch (cp.getBusStopId()) {
            case 0:
            case 2:
                cp.setBusStopId(3);
                cp.setAddressee(Id.moveBus);
                startContinualAssistant(cp);
                break;
            case 1:
                cp.setBusStopId(2);
                cp.setAddressee(Id.moveBus);
                startContinualAssistant(cp);
                break;
            case 3:
                cp.setBusStopId(4);
                cp.setAddressee(Id.moveBus);
                startContinualAssistant(cp);
                break;
            case 4:
                if (cp.getBus().isEmpty()) {
                    if (mySim().currentTime() > Globals.SIM_WARMANDOPEN_TIME) {
                        cp = (MyMessage) cp.createCopy();
                        cp.setAddressee(Id.agentService);
                        cp.setCode(Mc.serviceIsWorking);
                        request(cp);
                    } else {
                        cp.setBusStopId(0);
                        cp.setAddressee(Id.moveBus);
                        startContinualAssistant(cp);
                    }
                } else {
                    cp.setBusStopId(1);
                    cp.setAddressee(Id.moveBus);
                    startContinualAssistant(cp);
                }
                break;
        }
    }

    @Override
    public AgentBus myAgent() {
        return (AgentBus) super.myAgent();
    }
}
