package managers;

import OSPABA.*;
import simulation.*;
import agents.*;

//meta! id="2"
public class ManagerSur extends Manager {

    int inCounter;
    int outCounter;

    public ManagerSur(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        inCounter = 0;
        outCounter = 0;
    }

    //meta! sender="AgentBoss", id="10", type="Notice"
    public void processInit(MessageForm message) {
        MessageForm cp = message.createCopy();
        cp.setAddressee(Id.arrivalT1);
        startContinualAssistant(cp);
        cp = message.createCopy();
        cp.setAddressee(Id.arrivalT2);
        startContinualAssistant(cp);
        cp = message.createCopy();
        cp.setAddressee(Id.arrivalRent);
        startContinualAssistant(cp);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message;
        switch (message.code()) {
            case Mc.exitCust:
                //odchod zakaznika                
                if (msg.getGroup().getQueue().peek().isReturnCar()) {
                    if (msg.getGroup().getQueue().peek().getIncomeTime() > Globals.SIM_WARMING_TIME) {
                        myAgent().getTimeInSystemOut().addSample(mySim().currentTime() - msg.getGroup().getQueue().peek().getIncomeTime());
                    }
                } else {
                    if (msg.getGroup().getQueue().peek().getIncomeTime() > Globals.SIM_WARMING_TIME) {
                        myAgent().getTimeInSystemIn().addSample(mySim().currentTime() - msg.getGroup().getQueue().peek().getIncomeTime());
                    }
                }
                break;

            case Mc.finish:
                switch (message.sender().id()) {
                }
                break;

            case Mc.init:
                processInit(message);
                break;

            case Mc.arrivalCust:
                switch (message.sender().id()) {
                    case Id.arrivalT1:
                        MyMessage aT1 = (MyMessage) message.createCopy();
                        aT1.setCode(Mc.arrivalCustT1);
                        aT1.setAddressee(Id.agentTerminal);
                        notice(aT1);
                        break;

                    case Id.arrivalT2:
                        MyMessage aT2 = (MyMessage) message.createCopy();
                        aT2.setCode(Mc.arrivalCustT2);
                        aT2.setAddressee(Id.agentTerminal);
                        notice(aT2);
                        break;

                    case Id.arrivalRent:
                        MyMessage aR = (MyMessage) message.createCopy();
                        aR.setCode(Mc.arrivalCustRent);
                        aR.setAddressee(Id.agentService);
                        notice(aR);
                        break;
                }
        }
    }
    //meta! tag="end"

    @Override
    public AgentSur myAgent() {
        return (AgentSur) super.myAgent();
    }
}
