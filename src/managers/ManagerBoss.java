package managers;

import OSPABA.*;
import simulation.*;
import agents.*;

//meta! id="1"
public class ManagerBoss extends Manager {

    public ManagerBoss(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        MessageForm cp = message.createCopy();
        switch (message.code()) {
            case Mc.init:
                cp.setAddressee(Id.agentSur);
                notice(cp);
                cp = message.createCopy();
                cp.setAddressee(Id.agentBus);
                notice(cp);
                break;

            case Mc.arrivalCustRent:
                cp.setAddressee(Id.agentService);
                notice(cp);
                break;

            case Mc.arrivalCustT1:
            case Mc.arrivalCustT2:
                cp.setAddressee(Id.agentRental);
                notice(cp);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentBoss myAgent() {
        return (AgentBoss) super.myAgent();
    }
}
