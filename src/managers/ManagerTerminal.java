package managers;

import OSPABA.*;
import OSPDataStruct.SimQueue;
import simulation.*;
import agents.*;
import entities.Group;
import entities.Minibus;

//meta! id="20"
public class ManagerTerminal extends Manager {

    public ManagerTerminal(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        MyMessage msg = (MyMessage) message;
        switch (message.code()) {
            case Mc.arrivalCustT1:
                if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                    myAgent().getTerm1().addSample(myAgent().getSizeT1());
                }
                myAgent().getTerminal1().add(msg.getGroup());
                msg.getGroup().setQueueStartTime(mySim().currentTime());
                if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                    myAgent().getTerm1().addSample(myAgent().getSizeT1());
                }
                //System.out.println("T1 Cust time: " + msg.getGroup().getQueue().peek().getIncomeTime());
                break;

            case Mc.arrivalCustT2:
                if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                    myAgent().getTerm2().addSample(myAgent().getSizeT2());
                }
                myAgent().getTerminal2().add(msg.getGroup());
                msg.getGroup().setQueueStartTime(mySim().currentTime());
                if (mySim().currentTime() > Globals.SIM_WARMING_TIME) {
                    myAgent().getTerm2().addSample(myAgent().getSizeT2());
                }
                //System.out.println("T2 Cust time: " + msg.getGroup().getQueue().peek().getIncomeTime());
                break;

            case Mc.getCust:
                Group g = null;
                if (msg.getBusStopId() == 3) {
                    //terminal 2
                    g = findProperGroup(msg.getBus(), myAgent().getTerminal2());
                    if (g!=null && g.getQueueStartTime()>Globals.SIM_WARMING_TIME){
                        myAgent().getTermTime2().addSample(mySim().currentTime()-g.getQueueStartTime());
                    }
                } else {
                    //terminal 1
                    //System.out.println(myAgent().getTerminal1().size());
                    g = findProperGroup(msg.getBus(), myAgent().getTerminal1());
                    if (g!=null && g.getQueueStartTime()>Globals.SIM_WARMING_TIME){
                        myAgent().getTermTime1().addSample(mySim().currentTime()-g.getQueueStartTime());
                    }
                    //System.out.println(myAgent().getTerminal1().size());
                }
                msg.setGroup(g);
                msg.setCode(Mc.custToBus);
                response(msg);
                break;

            case Mc.busOutFinish:
                msg.setCode(Mc.exitCust);
                msg.setAddressee(Id.agentSur);
                notice(msg);
                break;
        }
    }
    //meta! tag="end"

    public Group findProperGroup(Minibus bus, SimQueue q) {
        Group g = null;
        if (q.size() > 0) {
            for (int i = 0; i < q.size(); i++) {
                g = (Group) q.get(i);
                if (g.getQueue().size() <= bus.getFreeSize()) {
                    q.remove(g);
                    return g;
                }
            }
        } else {
            return null;
        }
        return g;
    }

    @Override
    public AgentTerminal myAgent() {
        return (AgentTerminal) super.myAgent();
    }
}
